#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author : Quentin MICHAUD (quentin.michaud@telecom-sudparis.eu)
#
# Python PARSER for MiniPNG black and white images
# for personal interpretations when the specification is unclear, see the choices made in remarks.txt

from sys import argv
import os
from string import printable

def check_header(data):
	return data[:8] == b"Mini-PNG"

def parse_blocks(data):
	pointer = 8
	pixel_type = -1
	width = -1
	height = -1
	pixel_data = b""
	while pointer < len(data):
		block_type = data[pointer]
		pointer += 1
		block_len = int.from_bytes(data[pointer:pointer + 4], "big")
		pointer += 4
		block_data = data[pointer:pointer + block_len]
		pointer += block_len
		if block_type == ord("H"):
			width = int.from_bytes(block_data[:4], "big")
			height = int.from_bytes(block_data[4:8], "big")
			pixel_type = block_data[8]
			print("Width : %d" % width)
			print("Height : %d" % height)
			if pixel_type == 0:
				print("Pixel type : 0 (black and white)")
			elif pixel_type == 1:
				print("Pixel type : 1 (greyscale)")
			elif pixel_type == 3:
				print("Pixel type : 3 (RGB 24 bits)")
			else:
				print("[-] Unsupported pixel type")
				exit(1)
		elif block_type == ord("C"):
			for c in block_data:
				if chr(c) not in printable:
					print("[-] Unpritable comment.")
					exit(1)
			print("Comments : %s" % block_data.decode("ASCII"))
		elif block_type == ord("D"):
			pixel_data += block_data
		else:
			print("[-] Unknown block type.")
			exit(1)
	if pixel_type == -1:
		print("[-] No header block.")
		exit(1)
	if pixel_type == 0 and len(pixel_data) != ((width * height) + 7) // 8 \
	or pixel_type == 1 and len(pixel_data) != width * height \
	or pixel_data == 3 and len(pixel_data) == width * height * 3:
		print("[-] Wrong data size.")
		exit(1)
	return pixel_type, pixel_data, width, height

def display_image(pixel_type, pixel_data, width, height):
	if pixel_type == 0:
		pixel_data_bits = "".join([bin(data_oct)[2:].zfill(8) for data_oct in pixel_data])
		for i in range(height):
			for j in range(width):
				if pixel_data_bits[width * i + j] == "0":
					print("X", end="")
				else:
					print(" ", end="")
			print()
	elif pixel_type == 1:
		for i in range(height):
			for j in range(width):
				color = pixel_data[height * i + j]
				print_color(color, color, color)
			print()
	elif pixel_type == 3:
		for i in range(height):
			for j in range(width):
				pos = 3 * (width * i + j)
				r = pixel_data[pos]
				g = pixel_data[pos + 1]
				b = pixel_data[pos + 2]
				print_color(r, g, b)
			print()
	return None

def print_color(r, g, b):
	print("\033[38;2;{};{};{}mX\033[38;2;255;255;255m".format(r, g, b), end="")
	return None

def main():
	if len(argv) != 2:
		print("[!] Usage : %s <fichier>" % argv[0])
		exit(1)

	if not os.path.isfile(argv[1]):
		print("[!] %s is not a valid file." % argv[1])
		exit(1)
	
	print("Reading...")

	try:
		with open(argv[1], "rb") as f:
			data = f.read()
	except:
		print("[-] Error when opening file. Check permissions.")
		exit(1)

	if not check_header(data):
		print("Wrong header. Quitting.")
		exit(1)

	pixel_type, pixel_data, width, height = parse_blocks(data)
	display_image(pixel_type, pixel_data, width, height)


if __name__ == "__main__":
	main()
